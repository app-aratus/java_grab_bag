
public class Entry2 implements Comparable<Entry2> {
	
	public String name;
	public String phone;
	public double height;
	
	Entry2(String name, String phone, double height ) {
		this.name = name;
		this.phone = phone;
		this.height = height;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
        public double getHeight() {
                return height;
        }
        public void setHeight(double height) {
                this.height = height;
        }
	
	public void print() {
		System.out.println( "name: " + name + "   " + phone + "   " + height );
	}

	
       
        /*
        // works
	@Override
	public int compareTo(Entry2 entry) {
            int lastCmp = name.compareTo( entry.name );
            return ( lastCmp != 0 ? lastCmp : name.compareTo(entry.name) ); // if last comp != 0
	}
        */ 
	
        // works
	@Override
	public int compareTo(Entry2 entry) {
            return Double.compare(height, entry.height);
	}


	
}




