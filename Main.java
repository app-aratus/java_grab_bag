import java.util.TreeMap;
import java.util.Stack;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Stream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Collections;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;

public class Main {
	
    public static void main( String[] args ) {
        new Main();    
    }

    Main() {
        System.out.println("\nStarting Grab Bag");

        System.out.println(" ");
        System.out.println("Data structures:  a way to organize data with set relationships so that ");
        System.out.println("it can be effectively manipulated.  Linear data structures:  Array, Linked ");
        System.out.println("List, Queue (First In First Out), Stack (Last In First Out).  Non-linear");
        System.out.println("data structures:  Tree, Graph.");
        System.out.println(" ");
        
        showArrays();
        showVector();
        showTreeMap();
        showLinkedList();
        showStack();
        showHashSetAndHashTable();

        // Algorithms / traversals.
        // Divide and conquer traversal algorithm
        showQuickSort();
        // O(V+E) worst case 
        showDepthFirstBreadthFirst();

        showLoops();
        showIO();
        showSort();
        showLambda();
        showObjectOrientedShapes();
        //showScanner();
        showPathsFind();
        showConcurrency();
        showReverse();
        showIsPrime();
        showFactorial();
        showFizzBuzz();
        showPalendrome();
    }

    public void showArrays() {
        // Arrays.sort
        System.out.println("\nStarting Show Arrays");
        String[] fruits = new String[] {"Pineapple","Apple", "Orange", "Banana"};
        Arrays.sort(fruits);

        int i2=0;
        for(String temp: fruits){
            System.out.println("fruits " + ++i2 + " : " + temp);
        }

        // Collections.sort
        List<Entry> entries = new ArrayList<Entry>();

        entries.add( new Entry("Xavier",   "415-222-7543") );
        entries.add( new Entry("Sally",    "406-235-8655") );
        entries.add( new Entry("Adam",     "345-432-4567") );
        entries.add( new Entry("Zephron",  "408-432-7878") );
        entries.add( new Entry("Bill",     "303-333-1234") );
        entries.add( new Entry("Bart",     "720-456-1457") );

        System.out.println("\nUnsorted");
        for (int i = 0; i < entries.size(); i++) {
            Entry currentEntry = (Entry)entries.get(i);
            currentEntry.print();
        }

        System.out.println(" ");

        Collections.sort(entries);
        System.out.println("Sorted");

        for (int i = 0; i < entries.size(); i++) {
            Entry currentEntry = (Entry)entries.get(i);
            currentEntry.print();
        }

    }

    public void showVector() {
        System.out.println("\nStarting Show Vector");
        int counter = 0;

        System.out.println( " " );
        Vector<String> myVector = new Vector<String>();
        myVector.addElement("Banana");
        myVector.addElement("Cherry");
        myVector.addElement("Apple");
        myVector.addElement("Plum");
        myVector.addElement("Raspberry");

        for ( int i=0;i< myVector.size();++i) {
            ++counter;
            String data = (String)myVector.elementAt(i);
            System.out.println( counter + "  " + data);
        }

    }

    public void showTreeMap() {
        System.out.println("\nStarting Show TreeMap");
        StringBuffer stringBuffer = new StringBuffer();
        String  lineSeparator   = System.getProperty("line.separator");
        
        TreeMap<String, String> tm  = new TreeMap<String, String>();
        tm.put("zzz", "Z line");
        tm.put("mmm", "M line");
        tm.put("000", "0 line");
        tm.put("111", "1 line");
        tm.put("aaa", "A line");
        tm.put("bbb", "B line");
        tm.put("ccc", "C line");

        for ( Iterator<String> myIterator = tm.keySet().iterator(); myIterator.hasNext(); ) {
            String key   =   (String)myIterator.next();
            String value =   (String)tm.get(key);
            stringBuffer.append( "Key:  " + key + "   Value:  " + value + lineSeparator);
        }

        System.out.println(stringBuffer.toString());

    }

    public void showLinkedList() {
        System.out.println("\nStarting Show LinkedList");

        // List / LinkedList
        LinkedList<String> myLinkedList = new LinkedList<String>();
        myLinkedList.add("aa");
        myLinkedList.add("bb");
        myLinkedList.add("cc");
        myLinkedList.add("dd");
        myLinkedList.add("ee");
        myLinkedList.add("ff");
        myLinkedList.add("gg");

        int counter = 0;
        for ( Iterator<String> myIterator = myLinkedList.iterator();myIterator.hasNext(); ) {
            ++counter;
            String  key    = (String)myIterator.next();
            System.out.println( counter + "  " + key);
        }
    }

    public void showStack() {
        System.out.println("\nStarting Show Stack");
        //Stack<String>  stack;
        Stack stack = new Stack<String>();
        stack.push("aaa");
        stack.push("bb");
        stack.push("cc");
        
        while(!stack.empty()) {
            System.out.println(stack);
            stack.pop();
        }
    }

    public void showHashSetAndHashTable() {
        System.out.println("\nStarting ShowHashSetAndHashTable");
        // Set / HashSet
        int counter = 0;
        HashSet<String> types = new HashSet<String>();
        types.add("animal");    // 1
        types.add("animal");    // 2
        types.add("vegetable"); // 3
        types.add("mineral");   // 4

        counter = 0;
        for ( Iterator<String> myIterator = types.iterator();myIterator.hasNext(); ) {
            ++counter;
            String  key    = (String)myIterator.next();
            System.out.println( counter + "  " + key);
        }

        // Map / Hashtable
        System.out.println( " " );
        Hashtable<String, String> phoneBook = new Hashtable<String, String>();
        phoneBook.put("303-444-1234", "Bill Jones");      // 1
        phoneBook.put("720-848-4945", "Jim Sanchez");     // 2
        phoneBook.put("415-999-3456", "Beth Reynolds");   // 3
        phoneBook.put("408-886-5865", "Melissa Arnold");  // 4
        counter = 0;
        for ( Iterator<String> myIterator = phoneBook.keySet().iterator();myIterator.hasNext(); ) {
            ++counter;
            String  key    = (String)myIterator.next();
            String  value  = (String)phoneBook.get( key );
            System.out.println( counter + "  " + key + "   " + value );
        }
    }

    public static void quickSort(int[] arr, int start, int end){
        int partition = partitionQuickSort(arr, start, end);
        if (partition -1 > start) {
            quickSort(arr, start, partition - 1);
        }
        if (partition +1 < end) {
            quickSort(arr, partition + 1, end);
        }
    }

    public void showQuickSort() {
        System.out.println("\nStarting Show QuickSort");
        System.out.println("QuickSort is a divide-and-conquer algorithm.  It's 2-3 times faster ");
        System.out.println("than merge sort and heapsort.  We take the right most element and make");
        System.out.println("it the pivot.  Then put all the smaller numbers on left and all of the");
        System.out.println("larger numbers on the right.");
        System.out.println("                                                                       ");
        System.out.println(" Draw the pivot just for one row, then don't draw it any more.         ");
        System.out.println("                                                                       ");
        System.out.println("                       9 -3 5 2 6 8 -6 1 3                             ");
        System.out.println("                                         ^---Pivot                     ");
        System.out.println("                                                                       ");
        System.out.println("            -3 2 -6 1           3     8 5 9 6                          ");
        System.out.println("                    ^---Pivot               ^---Pivot                  ");
        System.out.println("                                                                       ");
        System.out.println("          -3 -6  1  2            5  6     9 8                          ");
        System.out.println("              ^---Pivot                     ^---Pivot                  ");
        System.out.println("                                                                       ");
        System.out.println("         -6  -3                           8 9                          ");
        System.out.println("                                                                       ");
        System.out.println("    Done-->          -6  -3  1  2  3  5  6  8  9                       ");
        System.out.println("                                                                       ");
        int[] arr = {100, 47, 3, 1, 88, 99, 23, 16, 58, 19};
        System.out.println("Array before:  " + Arrays.toString(arr));
        quickSort(arr, 0, arr.length-1);
        System.out.println("Array after:   " + Arrays.toString(arr));
    } 

    public void showDepthFirstBreadthFirst() {
        System.out.println("\nStarting Show DepthFirstBreadthFirst");
        System.out.println(" ");
        System.out.println("Trees are a type of graph called a connected acyclic graph.");
        System.out.println(" ");
        System.out.println("We will demonstrate 2 tree traversal algorithms:  a Breadth First,");
        System.out.println("and a Depth First. ");
        System.out.println(" ");
        System.out.println("Definitions: ");
        System.out.println(" ");
      //System.out.println("--------------------------------------------------------------------------------");
        System.out.println("Breadth:  The number of leaves.");
        System.out.println("Binary Tree:  A data structure in which each node has at most 2 children. ");
        System.out.println("Binary Search Tree:  A binary tree which has the following properties: ");
        System.out.println("  -The left subtree of a node contains only nodes with keys lesser than the ");
        System.out.println("  node. ");
        System.out.println("  -The right subtree of a node contains only nodes with keys greater than the");
        System.out.println("  node. ");
        System.out.println("  -Left and right subtrees must also be binary trees. ");
        System.out.println("Child:  A descendant node.");
        System.out.println("Depth:  The distance between a node and the root. ");
        System.out.println("Edge:  The connection between one node and another node.");
        System.out.println("Height:  The number of edges on the longest path between a node and a");
        System.out.println("descendant leaf.");
        System.out.println("Internal Node:  A node with at least one child.");
        System.out.println("Leaf:  A node with no children.");
        System.out.println("Level:  The number of edges between a node and the root + 1.");
        System.out.println("Parent:  The ancestor of a child node.");
        System.out.println("Root:  The top node in a tree, the prime ancestor.");
        System.out.println("Sub Tree:  A tree consisting of a node and all of its descendants.");
        System.out.println("Tree:  A hierarchical data structure which stores the information. A tree ");
        System.out.println("contains nodes(data) and connections(edges) which can not form a cycle.");
        System.out.println(" ");
        System.out.println("Tree Traversal:  A form of a graph traversal and refers to the process of  ");
        System.out.println("visiting (checking and updating) each node in the tree data structure only once.");
        System.out.println("The tree traversal is classified by the order of which it visits the nodes. ");
        System.out.println("There are 2 main categories: ");
        System.out.println("DFS Depth First Search Traversal:  Go deep into the nodes first.  There are 3");
        System.out.println("sub-categories:");
        System.out.println("    -Inorder Traversal:  Left subtree, node, right subtree.");
        System.out.println("    -Preorder Traversal:  Node, left subtree, right subtree.");
        System.out.println("    -Postorder Traversal:  Left subtree, right subtree, node.");
        System.out.println("BFS Breadth First Search Traversal:  Go horizontal left to right, then down.");
        System.out.println("    -Level Order Traversal:  Visit each horizontal level before going to the next");
        System.out.println("    down.  Go left to right. ");
        System.out.println(" ");
        System.out.println("In this example, we will use a binary tree which means it can ");
        System.out.println("have no more than 2 children.  Each Node will hold a String.");
        System.out.println(" ");
        try {
            Node tree = sampleTree();
            
            System.out.println("Here's a visual representation of the tree.");

            System.out.println("                                       ");
            System.out.println("                    A                  ");
            System.out.println("                    |                  ");
            System.out.println("         ----------------------        ");
            System.out.println("         |                    |        ");
            System.out.println("         B                    E        ");
            System.out.println("         |                    |        ");
            System.out.println("    -----------          -----------   ");
            System.out.println("    |         |          |         |   ");
            System.out.println("    C         D          F         G   ");
            System.out.println("                                   |   ");
            System.out.println("                                   H   ");

            System.out.println(" ");
            System.out.println("Breadth-First Search:");
            System.out.println(" ");
            System.out.println("We will use a linked list queue (first in first out) data structure. ");
            System.out.println(" ");
            BreadthFirstSearch.traversal(tree);

            System.out.println(" ");
            System.out.println("Depth-First Search:");
            DepthFirstSearch.traversal(tree);
            System.out.println(" ");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLoops() {
        // For loop
        System.out.println("\nStarting Show Loops");
        String aLine = "zero one two three";
        String[] aLineSplit = aLine.split(" ");
        for( int j = 0; j < aLineSplit.length; j++) {
            System.out.println(j + " = " + aLineSplit[j]);
        }

        //
        TreeSet<String> ts = new TreeSet<String>();
        ts.add("Colorado");
        ts.add("Wyoming");
        ts.add("Texas");
        ts.add("Utah");
        ts.add("Nevada");
        System.out.println(ts);
        for (String s: ts)
            System.out.println(s);
    }

    public void showIO() {
        System.out.println("\nStarting Show IO");
        try {
            BufferedReader bufferedReader = Files.newBufferedReader( Paths.get( "data.csv" ), Charset.forName("UTF-8"));
            String aLine;
            while ( (aLine = bufferedReader.readLine()) != null ) {
                System.out.println(aLine);
            }
        } catch (IOException e) {
            System.out.println( e.toString() );
        }
    }

    public void showSort() {
        System.out.println("\nStarting Show Sort");
        String[] fruits = new String[] {"Pineapple", "Apple", "Orange", "Banana"};
        Arrays.sort(fruits);
        int i2=0;
        for (String temp: fruits) {
            System.out.println("fruits " + ++i2 + " : " + temp);
        }
    }

    private static int[] multiply(int[] data, int multiple) {
        return Arrays.stream(data)
                .map(i -> i * multiple)
                .toArray();
    }

    private static int[] combine(int[] data1, int[] data2) {
        assert data1.length == data2.length;
        return IntStream.range(0, data1.length)
                .map(i -> data1[i] + data2[i])
                .toArray();
    }

    private static int sum(int[] data) {
        return Arrays.stream(data).sum();
    }

    private static int[] doUnaryOperation(int[] data, IntUnaryOperator op) {
        return Arrays.stream(data)
                .map(op)
                .toArray();
    }

    private static int[] oddOnly(int[] data) {
        return Arrays.stream(data)          // convert array to stream
                .filter(i -> i % 2 != 0)    // filter for all odd elements
                .toArray();                 // convert back to array
    }

    public void showLambda() {
        System.out.println("\nStarting Show Lambda");
        System.out.println("What is a Lambda Expression?");
        System.out.println("A lambda expression represents an anonymous function. It comprises of a set of");
        System.out.println("parameters, a lambda operator (->) and a function body.");
        
        ArrayList<String> names = new ArrayList<String>();
        names.add("Bob");
        names.add("Tom");
        names.add("Jeff");
        names.add("Zelda");
        names.add("Scott");
        names.add("J");
        names.add("JJ");
        names.add("JJJ");
        names.add("Jennifer");
        names.add("Steve");
        names.add("Janice");
        names.add("Jeffrey");
        names.add("Justin");
        names.add("Bert");

        ArrayList<String> cities = new ArrayList<String>();
        cities.add("San Francisco");
        cities.add("Sacramento");
        cities.add("Sydney");
        cities.add("New York");
        cities.add("Denver");
        cities.add("Littleton");
        cities.add("Colorado Springs");
        cities.add("Brush");
        cities.add("Cheyenne");
        cities.add("Boulder");
        cities.add("Fort Collins");
        cities.add("Paris");
        cities.add("Moscow");
        cities.add("Boston");
        cities.add("Miami");


        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);
        numbers.add(8);
        numbers.add(9);
        numbers.add(10);

        int[] myInts = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};


        // Map names
        System.out.println( "\nMap" );
        Stream<String> namesStream1 = names.stream()
                .map(s -> "PERSON:  " + s.toUpperCase() );
        String[] namesMap1 = namesStream1.toArray(String[]::new);
        System.out.println("namesMap1  " + Arrays.toString(namesMap1) );

        // Filter names, then sort them
        System.out.println( "\nFilter" );
        Stream<String> namesStream2 = names.stream()
                                .filter(p -> p.length() > 2)
                                .filter(p -> p.startsWith("J"))
                                .sorted(String::compareToIgnoreCase);
        // Convert back to an array.
        String[] namesFiltered1 = namesStream2.toArray(String[]::new);
        System.out.println("namesFiltered1  " + Arrays.toString(namesFiltered1) );


        // Filter names, then sort them
        System.out.println( "\nSort" );
        Stream<String> namesStream3 = names.stream()
                                .filter(p -> p.length() > 2)
                                .sorted(String::compareToIgnoreCase);
        // Convert back to an array.
        String[] namesFiltered2 = namesStream3.toArray(String[]::new);
        System.out.println("namesFiltered2  " + Arrays.toString(namesFiltered2) );

        System.out.println("\nOdd only: " + Arrays.toString(oddOnly(myInts)));

        System.out.println("\nMultiplied by 2: " + Arrays.toString(multiply(myInts, 2)));

        System.out.println("\nSum: " + sum(myInts));

        Stream<Integer> s1 = numbers.stream()
                        .filter(p -> p > 5);

    }

    public void showObjectOrientedShapes() {
        System.out.println("\nStarting Show Object Oriented Shapes");
        
        Rectangle rectangle = new Rectangle( 20.0, 40.0 );
        System.out.println(" ");
        System.out.println("Rectangle " + rectangle.get() );
        
        System.out.println(" ");
        Square square = new Square( 20.0 );
        System.out.println("Square " + square.get() );
        
        System.out.println(" ");
        Circle circle = new Circle( 20.0 );
        System.out.println("Circle " + circle.get() );
    }

    public void showScanner() {
        System.out.println("\nStarting Show Scanner");
        System.out.println(" ");
        Scanner sc = new Scanner(System.in);
        
        //System.out.print("Loops:  ");
        //int n = sc.nextInt();
        int n=1;
        for(int t = 0; t < n; t++) {
            System.out.print("First integer number:   ");
            int a = sc.nextInt();
            System.out.print("Second integer number:  ");
            int b = sc.nextInt();
            System.out.println(a+b);
        }

    }

    public void showPathsFind() {
        System.out.println("\nStarting Paths Find");
        var dirName = "/data";
        
        try (Stream<Path> paths = Files.walk(Paths.get(dirName), 2)) {

           //Files.walk(Paths.get(dirName))
           //.filter(Files::isRegularFile)
           //.forEach(System.out::println);

           paths.map(path -> path.toString()).filter(f -> f.endsWith(".java"))
                    .forEach(System.out::println);
        } catch (IOException e) {
        }
    }

    public void showConcurrency() {
        System.out.println("\nStarting Show Concurrency");
    	//ExecutorService executor = Executors.newSingleThreadExecutor(); // Single worker thread unbound queue.  One thread at a time.  Slowest.
        //ExecutorService executor = Executors.newFixedThreadPool(5); // Reuse fixed number of threads.  Swallows this many at a time.  Slow.
        ExecutorService executor = Executors.newCachedThreadPool(); // Creates new threads as needed.  Fast.

        for (int i = 0; i < 15; i++) {
            Runnable worker = new WorkerThread("" + i);
            executor.execute(worker);
        }
        executor.shutdown();
        
        while ( !executor.isTerminated() ) {
        }
        
        System.out.println("Finished all threads");

    }

    public static String reverse( String s ) {
        StringBuffer dataString      = new StringBuffer();
        for(int i = s.length() - 1; i >= 0; i--) {
            dataString = dataString.append( s.charAt(i) );
        }
        return dataString.toString();
    }

    public void showReverse() {
        // 
        System.out.println("\nStarting Show Reverse");
        System.out.println("abcd:  " + reverse("abcd"));
    }

    public static boolean isPrime(int number){
        for (int i=2; i<number; i++) {
            if (number%i == 0) {
                return false; //number is divisible so its not prime
            }    
        }   
        return true; //number is prime now 
    } 

    public void showIsPrime() {
        // 
        System.out.println("\nStarting Show Is Prime");
        System.out.println("4:  " + isPrime(4));
        System.out.println("5:  " + isPrime(5));
    }

    public void showFactorial() {
        // 
        System.out.println("\nStarting Show Factorial");
        System.out.println("5:  " + factorial(5));
    }

    public void showFizzBuzz() {
       /*
       Write a program that prints the numbers from 1 to 50. But for multiples of three print "Fizz" instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of both three and five print "FizzBuzz". 
       */
        for (int i = 1; i <= 50; i++) {
            if (i % (3*5) == 0) {
                System.out.println("FizzBuzz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else {
                System.out.println(i);
            }
        } 
    } 
  
    public static int factorial(int number) {
        int result = 1;
        while(number != 0){
            result = result*number;
            number--;
        }   
        return result;
    } 

    public void showPalendrome() {
        isPalendrome("Sally");
        isPalendrome("Jim");
        isPalendrome("bob");
        isPalendrome("xyxyxy");
        isPalendrome("cocoococ");
    } 
    
    public boolean isPalendrome(String word) {
        String reverse = new StringBuffer(word).reverse().toString();        
        if ( word.equals(reverse) ) {
            System.out.println("Word:  " + word + " is palendrome");
            return true;
        } else {
            System.out.println("Word:  " + word + " is not palendrome");
            return false;
        }
    } 

    public static int partitionQuickSort(int[] arr, int start, int end){
        int pivot = arr[end];
 
        for (int i=start; i < end; i++){
            if (arr[i]<pivot){
                int temp= arr[start];
                arr[start]=arr[i];
                arr[i]=temp;
                start++;
            }
        }
 
        int temp = arr[start];
        arr[start] = pivot;
        arr[end] = temp;
 
        return start;
    }

    private static Node sampleTree() {
        // A's right side (start with the lowest nodes on the tree)
        Node h    = new Node("H",    null, null);
        Node g    = new Node("G",    h,    null);
        Node f    = new Node("F",    null, null);
        Node e    = new Node("E",    f,    g   );

        // A's left side (start with the lowest nodes on the tree)
        Node c    = new Node("C",    null, null);
        Node d    = new Node("D",    null, null);
        Node b    = new Node("B",    c,    d   );

        // Combine A's left and right side
        Node root = new Node("A",    b,    e   );
        return root;
    }

}


