package com.apparatus.utilities;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.Set;
import java.util.List;
import com.apparatus.entity.Entry;
import com.apparatus.utilities.Utilities;

public class Utilities {

	public static final String lineSeparator = System.getProperty("line.separator");

	public static String printArray( String[] args ) {                      

		StringBuffer dataString      = new StringBuffer();

		dataString.append( lineSeparator );

		for ( int i = 0; i < args.length; ++i ) {
			dataString.append( (i + 1) + " " + args[i] + " " + lineSeparator );
		}

		return dataString.toString();
	}

	public static String printTreeMap( TreeMap<String, String> hashtable, boolean displayKeysOnly ) {

		StringBuffer dataString      = new StringBuffer();

		dataString.append( lineSeparator );

		for ( Iterator<String> myIterator = hashtable.keySet().iterator(); myIterator.hasNext(); ) {

			String key   =   (String)myIterator.next();     

			if ( displayKeysOnly == true ) {
				dataString.append( key + lineSeparator );
			} else {
				String value =   (String)hashtable.get(key);
				dataString.append( key  );
				dataString.append( "  Value:  " + value  );
				dataString.append( lineSeparator             );
			}

		}

		return dataString.toString();
	}

	public static String printSet( Set<String> set ) {

		StringBuffer dataString      = new StringBuffer();

		dataString.append( lineSeparator );

		dataString.append( lineSeparator );


		for ( Iterator<String> myIterator = set.iterator() ; myIterator.hasNext(); ) {

			String key   =   (String)myIterator.next();     

			dataString.append( key + lineSeparator );

		}

		dataString.append( lineSeparator );

		return dataString.toString();
	}
	
	public static String printEntriesList( List<Entry> entries ) {

		StringBuffer dataString      = new StringBuffer();

		for ( int i = 0; i < entries.size(); ++i ) {

            Entry currentEntry = (Entry)entries.get(i);
			dataString.append( (i + 1) + " " + currentEntry.printString() + " " + lineSeparator );

		}
		
		return dataString.toString();
	}

}


