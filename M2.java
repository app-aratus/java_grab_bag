import java.util.TreeMap;
import java.util.Stack;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.Comparator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Stream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.stream.IntStream;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Collections;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedList;

public class M2 {
	
    public static void main( String[] args ) {
        new M2();    
    }

    public void doIt() {
        // Collections.sort
        List<Entry2> entries = new ArrayList<Entry2>();


        entries.add( new Entry2("Xavier",   "415-222-7543", 98333.333) );
        entries.add( new Entry2("Sally",    "406-235-8655", 75444.444) );
        entries.add( new Entry2("Adam",     "345-432-4567", 2348.555) );
        entries.add( new Entry2("Zephron",  "408-432-7878", 666.666) );
        entries.add( new Entry2("Bill",     "303-333-1234", 1777.777) );
        entries.add( new Entry2("Bart",     "720-456-1457", 4888.888) );
        entries.add( new Entry2("Bart",     "720-222-1457777", 9888.8899998) );

        System.out.println("\nUnsorted");
        for (int i = 0; i < entries.size(); i++) {
            Entry2 currentEntry2 = (Entry2)entries.get(i);
            currentEntry2.print();
        }

        System.out.println(" ");

        Collections.sort(entries);
        System.out.println("Sorted");

        for (int i = 0; i < entries.size(); i++) {
            Entry2 currentEntry2 = (Entry2)entries.get(i);
            currentEntry2.print();
        }


    }

    M2() {
        System.out.println(" ");
        
        doIt();
    }

}
