package com.apparatus.utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.charset.Charset;

public class FileIO {
	
	// write small strings to a file
	public static void writeStringToFile( String datatString, String fileName ) {
		try {
			Files.write( Paths.get( fileName ), datatString.getBytes() );
		} catch (IOException e) {
			System.out.println("Error in:  FileIO.writeStringToFile:  " + fileName);
			System.out.println( e.toString() );
			System.exit(0);	
		}
	}
	
	public static void bufferedWriteStringToFile( String dataString, String fileName ) {
		try (BufferedWriter writer = Files.newBufferedWriter( Paths.get(fileName), Charset.forName("UTF-8") ) ) {
			writer.write( dataString, 0, dataString.length() );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// read file as lines
	public static String[] readFileLinesToStringArray( String fileName ) {
		String[] data = null;
		try {
	        data = Arrays.asList( Files.lines( Paths.get(fileName) ).toArray() ).toArray(new String[0]);
		} catch (IOException e) {
			System.out.println("Error in:  FileIO.readFileLinesToStringArray:  " + fileName);
			System.out.println( e.toString() );
			System.exit(0);
		}
		return   data;
	}
	
	// buffered read file to string
	public static String bufferedReadFileToString( String fileName ) {
		StringBuffer stringBuffer   = new StringBuffer();
		String 		 lineSeparator  = System.getProperty("line.separator");
		try {
			BufferedReader bufferedReader = Files.newBufferedReader( Paths.get(fileName), Charset.forName("UTF-8"));
			String aLine = null;
            while ( (aLine = bufferedReader.readLine()) != null ) {
            	stringBuffer.append( aLine + lineSeparator );
			}
		} catch (IOException e) {
			System.out.println("Error in:  FileIO.bufferedReadFileToString:  " + fileName);
			System.out.println( e.toString() );
			System.exit(0);
		}
		return   stringBuffer.toString();
	}
	
}








