

public class WorkerThread implements Runnable {
  
    private String command;
    
    public WorkerThread(String s){
        this.command = s;
    }

    @Override
    public void run() {
    	// Do work here.
        System.out.println(Thread.currentThread().getName()+" Start. Command = " + command);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString(){
        return this.command;
    }
    
}


