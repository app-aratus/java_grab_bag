
public class Entry implements Comparable<Entry> {
	
	public String name;
	public String phone;
	
	Entry(String name, String phone ) {
		this.name = name;
		this.phone = phone;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public void print() {
		System.out.println( "name: " + name + "   " + phone );
	}

	
	@Override
	public int compareTo(Entry entry) {
        int lastCmp = name.compareTo( entry.name );
        return ( lastCmp != 0 ? lastCmp : name.compareTo(entry.name) ); // if last comp != 0
	}
	

	
}




