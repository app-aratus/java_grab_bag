# README #

This README is for a grab bag of useful Java code.

### What is this repository for? ###

* A diverse grab bag of useful Java code.
* 1.0

### How do I get set up? ###

* Summary of set up
  For Unix, execute run.sh.
* Dependencies
  Java 8 or higher.

### Who do I talk to? ###

* Repo owner
https://www.linkedin.com/in/morrisseals/

* Code output

./run.sh
rm *.class
javac Main.java
java Main

Starting Grab Bag
 
Data structures:  a way to organize data with set relationships so that 
it can be effectively manipulated.  Linear data structures:  Array, Linked 
List, Queue (First In First Out), Stack (Last In First Out).  Non-linear
data structures:  Tree, Graph.
 

Starting Show Arrays
fruits 1 : Apple
fruits 2 : Banana
fruits 3 : Orange
fruits 4 : Pineapple

Unsorted
name: Xavier   415-222-7543
name: Sally   406-235-8655
name: Adam   345-432-4567
name: Zephron   408-432-7878
name: Bill   303-333-1234
name: Bart   720-456-1457
 
Sorted
name: Adam   345-432-4567
name: Bart   720-456-1457
name: Bill   303-333-1234
name: Sally   406-235-8655
name: Xavier   415-222-7543
name: Zephron   408-432-7878

Starting Show Vector
 
1  Banana
2  Cherry
3  Apple
4  Plum
5  Raspberry

Starting Show TreeMap
Key:  000   Value:  0 line
Key:  111   Value:  1 line
Key:  aaa   Value:  A line
Key:  bbb   Value:  B line
Key:  ccc   Value:  C line
Key:  mmm   Value:  M line
Key:  zzz   Value:  Z line


Starting Show LinkedList
1  aa
2  bb
3  cc
4  dd
5  ee
6  ff
7  gg

Starting Show Stack
[aaa, bb, cc]
[aaa, bb]
[aaa]

Starting ShowHashSetAndHashTable
1  animal
2  mineral
3  vegetable
 
1  720-848-4945   Jim Sanchez
2  415-999-3456   Beth Reynolds
3  408-886-5865   Melissa Arnold
4  303-444-1234   Bill Jones

Starting Show QuickSort
QuickSort is a divide-and-conquer algorithm.  It's 2-3 times faster 
than merge sort and heapsort.  We take the right most element and make
it the pivot.  Then put all the smaller numbers on left and all of the
larger numbers on the right.
                                                                       
 Draw the pivot just for one row, then don't draw it any more.         
                                                                       
                       9 -3 5 2 6 8 -6 1 3                             
                                         ^---Pivot                     
                                                                       
            -3 2 -6 1           3     8 5 9 6                          
                    ^---Pivot               ^---Pivot                  
                                                                       
          -3 -6  1  2            5  6     9 8                          
              ^---Pivot                     ^---Pivot                  
                                                                       
         -6  -3                           8 9                          
                                                                       
    Done-->          -6  -3  1  2  3  5  6  8  9                       
                                                                       
Array before:  [100, 47, 3, 1, 88, 99, 23, 16, 58, 19]
Array after:   [1, 3, 16, 19, 23, 47, 58, 88, 99, 100]

Starting Show DepthFirstBreadthFirst
 
Trees are a type of graph called a connected acyclic graph.
 
We will demonstrate 2 tree traversal algorithms:  a Breadth First,
and a Depth First. 
 
Definitions: 
 
Breadth:  The number of leaves.
Binary Tree:  A data structure in which each node has at most 2 children. 
Binary Search Tree:  A binary tree which has the following properties: 
  -The left subtree of a node contains only nodes with keys lesser than the 
  node. 
  -The right subtree of a node contains only nodes with keys greater than the
  node. 
  -Left and right subtrees must also be binary trees. 
Child:  A descendant node.
Depth:  The distance between a node and the root. 
Edge:  The connection between one node and another node.
Height:  The number of edges on the longest path between a node and a
descendant leaf.
Internal Node:  A node with at least one child.
Leaf:  A node with no children.
Level:  The number of edges between a node and the root + 1.
Parent:  The ancestor of a child node.
Root:  The top node in a tree, the prime ancestor.
Sub Tree:  A tree consisting of a node and all of its descendants.
Tree:  A hierarchical data structure which stores the information. A tree 
contains nodes(data) and connections(edges) which can not form a cycle.
 
Tree Traversal:  A form of a graph traversal and refers to the process of  
visiting (checking and updating) each node in the tree data structure only once.
The tree traversal is classified by the order of which it visits the nodes. 
There are 2 main categories: 
DFS Depth First Search Traversal:  Go deep into the nodes first.  There are 3
sub-categories:
    -Inorder Traversal:  Left subtree, node, right subtree.
    -Preorder Traversal:  Node, left subtree, right subtree.
    -Postorder Traversal:  Left subtree, right subtree, node.
BFS Breadth First Search Traversal:  Go horizontal left to right, then down.
    -Level Order Traversal:  Visit each horizontal level before going to the next
    down.  Go left to right. 
 
In this example, we will use a binary tree which means it can 
have no more than 2 children.  Each Node will hold a String.
 
Here's a visual representation of the tree.
                                       
                    A                  
                    |                  
         ----------------------        
         |                    |        
         B                    E        
         |                    |        
    -----------          -----------   
    |         |          |         |   
    C         D          F         G   
                                   |   
                                   H   
 
Breadth-First Search:
 
We will use a linked list queue (first in first out) data structure. 
 
A 
B 
E 
C 
D 
F 
G 
H 
 
Depth-First Search:
A
B
C
D
E
F
G
H
 

Starting Show Loops
0 = zero
1 = one
2 = two
3 = three
[Colorado, Nevada, Texas, Utah, Wyoming]
Colorado
Nevada
Texas
Utah
Wyoming

Starting Show IO
line 1
line 2
line 3
line 4

Starting Show Sort
fruits 1 : Apple
fruits 2 : Banana
fruits 3 : Orange
fruits 4 : Pineapple

Starting Show Lambda
What is a Lambda Expression?
A lambda expression represents an anonymous function. It comprises of a set of
parameters, a lambda operator (->) and a function body.

Map
namesMap1  [PERSON:  BOB, PERSON:  TOM, PERSON:  JEFF, PERSON:  ZELDA, PERSON:  SCOTT, PERSON:  J, PERSON:  JJ, PERSON:  JJJ, PERSON:  JENNIFER, PERSON:  STEVE, PERSON:  JANICE, PERSON:  JEFFREY, PERSON:  JUSTIN, PERSON:  BERT]

Filter
namesFiltered1  [Janice, Jeff, Jeffrey, Jennifer, JJJ, Justin]

Sort
namesFiltered2  [Bert, Bob, Janice, Jeff, Jeffrey, Jennifer, JJJ, Justin, Scott, Steve, Tom, Zelda]

Odd only: [1, 3, 5, 7, 9]

Multiplied by 2: [2, 4, 6, 8, 10, 12, 14, 16, 18, 20]

Sum: 55

Starting Show Object Oriented Shapes
instantiating base class
Starting Rectangle
 
Rectangle 800.0
 
instantiating base class
Starting Square
Square 400.0
 
instantiating base class
Starting Circle
Circle 1256.6000000000001

Starting Paths Find
/data/aa.java

Starting Show Concurrency
pool-1-thread-1 Start. Command = 0
pool-1-thread-4 Start. Command = 3
pool-1-thread-13 Start. Command = 12
pool-1-thread-9 Start. Command = 8
pool-1-thread-3 Start. Command = 2
pool-1-thread-11 Start. Command = 10
pool-1-thread-15 Start. Command = 14
pool-1-thread-10 Start. Command = 9
pool-1-thread-8 Start. Command = 7
pool-1-thread-12 Start. Command = 11
pool-1-thread-7 Start. Command = 6
pool-1-thread-14 Start. Command = 13
pool-1-thread-5 Start. Command = 4
pool-1-thread-6 Start. Command = 5
pool-1-thread-2 Start. Command = 1
Finished all threads

Starting Show Reverse
abcd:  dcba

Starting Show Is Prime
4:  false
5:  true

Starting Show Factorial
5:  120
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
Fizz
Buzz
26
Fizz
28
29
FizzBuzz
31
32
Fizz
34
Buzz
Fizz
37
38
Fizz
Buzz
41
Fizz
43
44
FizzBuzz
46
47
Fizz
49
Buzz
Word:  Sally is not palendrome
Word:  Jim is not palendrome
Word:  bob is palendrome
Word:  xyxyxy is not palendrome
Word:  cocoococ is palendrome
